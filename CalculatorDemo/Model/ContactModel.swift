//
//  ContactModel.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/29/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class ContactModel: NSObject {
    var id: String?
    var name: String? {
           return (lastName ?? "") + " " + (firstName ?? "")
       }
    var phone: String?
    var avatar: String?
    var firstName: String?
    var lastName: String?
    var mail: String?
    var phoneWork: String?
    var address: String?
    var createDate: String?
    
    override init() {
        super.init()
    }

    init(dictionary: [String: Any]) {
        if let id = dictionary["_id"] as? String {
            self.id = id
        }
        if let firstName = dictionary["firstName"] as? String {
            self.firstName = firstName
        }
        if let lastName = dictionary["lastName"] as? String {
            self.lastName = lastName
        }
        if let avatar = dictionary["twitter"] as? String {
            self.avatar = avatar
        }
        if let mail = dictionary["mail"] as? String {
            self.mail = mail
        }
        if let phoneDict = dictionary["phone"] as? [String: Any], let phoneWork = phoneDict["work"] as? String {
            self.phoneWork = phoneWork
        }
        if let phoneDict = dictionary["phone"] as? [String: Any], let phone = phoneDict["mobile"] as? String {
            self.phone = phone
        }
        if let address = dictionary["address"] as? String {
            self.address = address
        }
        if let createDate = dictionary["createDate"] as? String {
            self.createDate = createDate
        }
    }
    
    func toDictionary() -> [String: Any] {
        
        var phoneDict = [String: Any]()
        phoneDict["mobile"] = phone ?? ""
        phoneDict["work"] = phoneWork ?? ""
        
        var dict = [String: Any]()
        dict["firstName"] = firstName ?? ""
        dict["lastName"] = lastName ?? ""
        dict["twitter"] = avatar ?? ""
        dict["mail"] = mail ?? ""
        dict["phone"] = phoneDict
        dict["address"] = address ?? ""
        
        return dict
    }
}
