//
//  PopupController.swift
//  CalculatorDemo
//
//  Created by unitech on 10/5/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class PopupController: NSObject {
    static func createPopup(with message: String) -> UIAlertController {
        let vc = UIAlertController(title: "Notification", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
            
        }
        vc.addAction(okAction)
        
        return vc
    }
    
    static func createConfirmPopup(with message: String, okHandler: ((UIAlertAction) -> Void)? = nil, cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let vc = UIAlertController(title: "Notification", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: okHandler)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelHandler)
        vc.addAction(okAction)
        vc.addAction(cancelAction)
        
        return vc
    }
}
