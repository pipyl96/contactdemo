//
//  APIController.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/4/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class APIController: NSObject {

    static let shared = APIController()
    
    func deleteContact(_ idContact: String, completion: @escaping (Error?) -> ()) {

        guard let url = URL(string: "https://loicontacts.herokuapp.com/contacts/\(idContact)") else {
            completion(createError(with: "URL not correct!"))
            return
        }
                
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "DELETE"
                
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(error)
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode >= 200, response.statusCode < 300 else {
                completion(self.createError(with: "An error occurred, please try again!"))
                return
            }
            completion(nil)
        }
        
        task.resume()
    }
    
    func addContact(_ contact: ContactModel, completion: @escaping (Error?) -> ()) {

        guard let url = URL(string: "https://loicontacts.herokuapp.com/contacts") else {
            completion(createError(with: "URL not correct!"))
            return
        }
        
        let params = contact.toDictionary()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        
        
        guard let data = try? JSONSerialization.data(withJSONObject: params, options: []), let jsonString = String(data: data, encoding: .utf8), let dataRequest = jsonString.data(using: .utf8) else {
            completion(createError(with: "Error HTTP Body!"))
            return
        }
        
        urlRequest.httpBody = dataRequest
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(error)
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 201 else {
                completion(self.createError(with: "An error occurred, please try again!"))
                return
            }
            completion(nil)
        }
        
        task.resume()
    }
    
    func getContacts(completion: @escaping(Result<[ContactModel], Error>) -> ()) {
        //tao 1 url va kiem tra no khac nil
        guard let url = URL(string: "https://loicontacts.herokuapp.com/contacts") else {
            completion(.failure(createError(with: "URL not correct!")))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data, let list = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
                completion(.failure(self.createError(with: "An error occurred, please try again!")))
                return
            }
            
            var contactList = [ContactModel]()
            
            for object in list {
                let contact = ContactModel(dictionary: object)
                contactList.append(contact)
            }
            
            completion(.success(contactList))
        }
        task.resume()
    }
    
    func getImage(urlString: String, completion: @escaping (UIImage?) -> Void) {
        if let avatar = ImagesController.shared.loadImageFromDiskWith(url: urlString) {
            completion(avatar)
            return
        }
        
        guard let url = URL(string: urlString)
            else {
                completion(nil)
                return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data, let image = UIImage(data: data) {
                completion(image)
                ImagesController.shared.saveImage(url: urlString, image: image)
                return
            }
            completion(nil)
            return
        }
        task.resume()
    }
    
    //MARK: - Helper
    private func createError(with message: String) -> NSError {
        let error = NSError(domain: "com.contactDemo", code: 404, userInfo: [NSLocalizedDescriptionKey: message])
        return error
    }
}
