//
//  HUDManager.swift
//  CalculatorDemo
//
//  Created by unitech on 10/5/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MBProgressHUD

class HUDManager: NSObject {
    static let shared = HUDManager()
    private var hud: MBProgressHUD?
    
    func show() {
        hud?.hide(animated: true)
        guard let window = UIApplication.shared.windows.last else { return }
        hud = MBProgressHUD.showAdded(to: window, animated: true)
        hud?.contentColor = UIColor.defaultColor
    }
    
    func dismiss() {
        DispatchQueue.main.async {
            self.hud?.hide(animated: true)
        }
    }
}
