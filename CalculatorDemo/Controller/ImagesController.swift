//
//  ImagesController.swift
//  CoronaInfomation
//
//  Created by unitech on 9/8/20.
//  Copyright © 2020 unitech. All rights reserved.
//

import UIKit

class ImagesController {
    
    static var shared = ImagesController()
    
    func saveImage(url: String, image: UIImage) {
        guard let imageName = getNameImageFromURL(url) else { return }
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
    }
    
    func loadImageFromDiskWith(url: String) -> UIImage? {
        guard let fileName = getNameImageFromURL(url) else { return nil }
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
    
    private func getNameImageFromURL(_ url: String) -> String? {
        let list = url.split(separator: "/")
        if let name = list.last {
            return String(name)
        }
        return nil
    }
}
