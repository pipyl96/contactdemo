//
//  SignInViewController.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/22/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    //Bien
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var parentUserNameView: UIView!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var parentPasswordView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var signInButton: UIButton! //Khai bao bien
    
    @IBOutlet weak var wellComeBackLabel: UILabel!
    @IBOutlet weak var signInLabel: UILabel!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setupUI() { //thiet lap giao dien
        avatarImageView.layer.cornerRadius = 70
        parentUserNameView.layer.cornerRadius = 15
        userNameView.layer.cornerRadius = 15
        parentPasswordView.layer.cornerRadius = 15
        passwordView.layer.cornerRadius = 15
    }
    
    func validateSignIn() { //kiem tra du lieu dang nhap
        if let userName = userNameTextField.text,
            let password = passwordTextField.text,
            userName != "",
            password != "",
            password.count >= 6 {
                print("Validate success: " + userName + " || " + password)
            } else {
                print("Validate fail")
        }
    }
    
    //Function
    @IBAction func didClickSignInButton(_ sender: Any) { //login tai khoan voi du lieu ma nguoi dung nhap vao
//        validateSignIn()
        
        //B1: tao ra 1 storyboard chua viewcontroller muon hien thi (Main.storyboard)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)

//        //B2: tao ra viewcontroller muon hien thi (ContactViewController)
        
        let contactViewController = mainStoryboard.instantiateViewController(withIdentifier: "ContactViewController")

//        //B3: thuc hien hien thi viewcontroller (ContactViewController)
        
        self.navigationController?.pushViewController(contactViewController, animated: true)
    }
}
