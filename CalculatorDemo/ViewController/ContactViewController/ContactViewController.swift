//
//  ContactViewController.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/26/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MBProgressHUD

class ContactViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var contactList = [ContactModel]()
    private var defaultContactList = [ContactModel]()
    private var refreshControl = UIRefreshControl()

    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getContacts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    //MARK: - Helper
    private func setupUI() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        tableView.contentInset = UIEdgeInsets(top: 6, left: 0, bottom: 6, right: 0)
        
        let addBarButton = UIBarButtonItem(image: UIImage(named: "ic_plus"), style: .done, target: self, action: #selector(didClickAddBarButton))
        self.navigationItem.rightBarButtonItem = addBarButton
        
        refreshControl.addTarget(self, action: #selector(didRefreshContacts), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func didRefreshContacts() {
        getContacts()
    }
    
    @objc func didClickAddBarButton() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddContactVC") as? AddContactVC {
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            resetContactList()
        }
    }
    
    private func deleteContactInList(with contact: ContactModel) {
        if let index = defaultContactList.firstIndex(of: contact) {
            defaultContactList.remove(at: index)
        }
        if let index = contactList.firstIndex(of: contact) {
            contactList.remove(at: index)
        }
        tableView.reloadData()
    }
    
    private func addContactToList(with contact: ContactModel) {
        defaultContactList.append(contact)
        defaultContactList = sortByABC(list: defaultContactList)
        resetContactList()
    }
    
    private func filterContacts(with keySearch: String) {
        if keySearch == "" {
            contactList = defaultContactList
        } else {
            contactList = contactList.filter { $0.name?.lowercased().contains(keySearch.lowercased()) ?? false }
        }
        tableView.reloadData()
    }
    
    private func resetContactList() {
        contactList = defaultContactList
        tableView.reloadData()
    }
    
    private func sortByABC(list: [ContactModel]) -> [ContactModel] {
        return list.sorted { $0.lastName!.lowercased() < $1.lastName!.lowercased() }
    }
    
    //MARK: - Actions
    @IBAction func didClickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - APIs
    private func deleteContact(with contact: ContactModel) {
        guard let idContact = contact.id else {
            return
        }
        HUDManager.shared.show()
        APIController.shared.deleteContact(idContact) { (error) in
            DispatchQueue.main.async {
                HUDManager.shared.dismiss()
                if let error = error {
                    self.present(PopupController.createPopup(with: error.localizedDescription), animated: true)
                    return
                }
                self.present(PopupController.createPopup(with: "Add contact successed"), animated: true)
                self.deleteContactInList(with: contact)
            }
        }
    }
    
    private func getContacts() {
        HUDManager.shared.show()
        APIController.shared.getContacts { [weak self] (result) in
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
            }
            HUDManager.shared.dismiss()
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                break
            case .success(let contactList):
                self.defaultContactList = self.sortByABC(list: contactList)
                self.contactList = self.defaultContactList
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

//MARK: - TableView
extension ContactViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contactModel = contactList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        cell.setupCell(contactModel: contactModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsVC") as! ContactDetailsVC
        vc.contact = contactList[indexPath.row]
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let contact = contactList[indexPath.row]
        let popup = PopupController.createConfirmPopup(with: "Do you want delete this contact?", okHandler: { (alertAction) in
            self.deleteContact(with: contact)
        }, cancelHandler: nil)
        self.present(popup, animated: true)
    }
}

//MARK: - AddContactVCDelegate
extension ContactViewController: AddContactVCDelegate {
    func didAddContact(with contact: ContactModel) {
        addContactToList(with: contact)
    }
}

//MARK: - UISearchBarDelegate
extension ContactViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContacts(with: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}
