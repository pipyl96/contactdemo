//
//  ContactTableViewCell.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/26/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        parentView.addShadow(color: .lightGray, radius: 5, opacity: 0.7, offset: .zero)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = UIImage(named: "ic_avatar")
    }
    
    func setupCell(contactModel: ContactModel) {
        nameLabel.text = contactModel.name
        phoneLabel.text = contactModel.phone
        
        getAvatar(with: contactModel.avatar)
    }
    
    @IBAction func didTapCallButton(_ sender: Any) {
        
    }
    
    //MARK: - APIs
    private func getAvatar(with avatar: String?) {
        guard let avatar = avatar, avatar != "" else {
            return
        }
        APIController.shared.getImage(urlString: avatar) { [weak self] (avatar) in
            guard let self = self else { return }
            if let avatar = avatar {
                DispatchQueue.main.async {
                    self.avatarImageView.image = avatar
                }
            }
        }
    }
}
