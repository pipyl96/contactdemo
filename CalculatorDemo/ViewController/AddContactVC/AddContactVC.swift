//
//  AddContactVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/4/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

protocol AddContactVCDelegate: class {
    func didAddContact(with contact: ContactModel)
}

class AddContactVC: UITableViewController {

    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var phoneWorkTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var avatarTF: UITextField!

    weak var delegate: AddContactVCDelegate?
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Helper
    private func setupUI() {
        self.navigationItem.title = "Add Contact"
        self.tableView.tableFooterView = UIView()
        
        let tapAddress = UITapGestureRecognizer(target: self, action: #selector(didClickSelectAddress))
        addressTF.addGestureRecognizer(tapAddress)
    }
    
    @objc func didClickSelectAddress() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapsVC") as? MapsVC {
            vc.delegate = self
            vc.isSelectedAddress = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func isValidate() -> Bool {
        var errorMessage = ""
        
        if !firstNameTF.hasText {
            errorMessage = "Vui lòng nhập tên!"
        } else if !lastNameTF.hasText {
            errorMessage = "Vui lòng nhập họ và tên đệm!"
        } else if !phoneTF.hasText {
            errorMessage = "Vui lòng nhập số điện thoại (cá nhân)!"
        } else if !emailTF.hasText {
            errorMessage = "Vui lòng nhập mail!"
        } else if !phoneTF.text!.isNumber || (phoneTF.text!.count != 10 && phoneTF.text!.count != 11) {
            errorMessage = "Vui lòng nhập đúng định dạng số điện thoại (cá nhân)!"
        } else if phoneWorkTF.hasText, !phoneWorkTF.text!.isNumber || (phoneWorkTF.text!.count != 10 && phoneWorkTF.text!.count != 11) {
            errorMessage = "Vui lòng nhập đúng định dạng số điện thoại (công việc)!"
        } else if !emailTF.text!.isValidEmail {
            errorMessage = "Vui lòng nhập đúng định dạng mail!"
        }
        
        if errorMessage != "" {
            let popup = PopupController.createPopup(with: errorMessage)
            self.present(popup, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    //MARK: - Actions
    @IBAction func didClickAddButton(_ sender: Any) {
        if !isValidate() {
            return
        }
        
        let contact = ContactModel()
        contact.mail = emailTF.text
        contact.firstName = firstNameTF.text
        contact.lastName = lastNameTF.text
        contact.address = addressTF.text
        contact.phone = phoneTF.text
        contact.phoneWork = phoneWorkTF.text
        contact.avatar = ["https://cdn.vox-cdn.com/thumbor/rES5fxTJl-z014NcJV7Rradtxrc=/0x86:706x557/1400x1400/filters:focal(0x86:706x557):format(png)/cdn.vox-cdn.com/imported_assets/847184/stevejobs.png", "https://upload.wikimedia.org/wikipedia/commons/b/b9/Steve_Jobs_Headshot_2010-CROP.jpg", "https://cdn1.i-scmp.com/sites/default/files/styles/1200x800/public/2015/05/12/timcook-conf.jpg?itok=ruPHDsOW"].randomElement()!//avatarTF.text
        
        addContact(with: contact)
    }
    
    //MARK: - APIs
    private func addContact(with contact: ContactModel) {
        HUDManager.shared.show()
        APIController.shared.addContact(contact) { (error) in
            HUDManager.shared.dismiss()
            DispatchQueue.main.async {
                if let error = error {
                    self.present(PopupController.createPopup(with: error.localizedDescription), animated: true)
                    return
                }
                self.delegate?.didAddContact(with: contact)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

//MARK: - MapsVCDelegate
extension AddContactVC: MapsVCDelegate {
    func didSelectAddress(with address: String) {
        addressTF.text = address
    }
}
