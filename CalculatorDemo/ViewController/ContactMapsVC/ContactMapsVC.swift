//
//  ContactMapsVC.swift
//  CalculatorDemo
//
//  Created by unitech on 10/6/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MapKit

class ContactMapsVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var contacts: [ContactModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getContacts()
    }
    
    //MAKR: - Handle Maps
    private func addAllMarker() {
        if let contacts = contacts {
            let queue = DispatchQueue.global()
            contacts.forEach {
                if let address = $0.address, address != "" {
                    let name = $0.name ?? ""
                    queue.async {
                        self.convertAddressToCoordinate(with: address, and: name)
                    }
                }
            }
        }
    }
    
    private func addMarker(location: CLLocation, address: String, name: String) {
        let marker = MKPointAnnotation()
        marker.coordinate = location.coordinate
        marker.title = name
        marker.subtitle = address
        mapView.addAnnotation(marker)
    }
    
    private func convertAddressToCoordinate(with address: String, and name: String) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
            self.addMarker(location: location, address: address, name: name)
        }
    }

    //MARK: - API
    private func getContacts() {
        HUDManager.shared.show()
        APIController.shared.getContacts { [weak self] (result) in
            HUDManager.shared.dismiss()
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                break
            case .success(let contactList):
                self.contacts = contactList
                self.addAllMarker()
            }
        }
    }
}
