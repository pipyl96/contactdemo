//
//  MapsVC.swift
//  CalculatorDemo
//
//  Created by unitech on 10/5/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MapKit

protocol MapsVCDelegate: class {
    func didSelectAddress(with address: String)
}

class MapsVC: UIViewController {

    @IBOutlet weak var mapsView: MKMapView!
    
    weak var delegate: MapsVCDelegate?
    var isSelectedAddress = false
    var address: String?
    
    private var selectedMarker = MKPointAnnotation()
    private var currentMarker: MKPointAnnotation?
    private let locationManager = CLLocationManager()

    //MAKR: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MAKR: - Helper
    private func setupUI() {
        self.navigationItem.title = "Maps"
        
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }

        if isSelectedAddress {
            mapsView.showsUserLocation = true
            let confirmButton = UIBarButtonItem(image: UIImage(named: "ic_confirm"), style: .done, target: self, action: #selector(didConfirmAddress))
            self.navigationItem.rightBarButtonItem = confirmButton
            return
        }
        
        if let address = address {
            convertAddressToCoordinate(with: address)
        }
    }
    
    @objc func didConfirmAddress() {
        if let address = address {
            delegate?.didSelectAddress(with: address)
            self.navigationController?.popViewController(animated: true)
        } else {
            self.present(PopupController.createPopup(with: "Vui lòng chọn địa chỉ!"), animated: true, completion: nil)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let point = touches.first?.location(in: mapsView), isSelectedAddress {
            let coordinate = mapsView.convert(point, toCoordinateFrom: mapsView)
            convertLocationToAddress(with: coordinate)
        }
    }
    
    //MAKR: - Handle Maps
    private func updateSelectedMarker(location: CLLocation) {
        selectedMarker.coordinate = location.coordinate
        selectedMarker.title = address
        mapsView.addAnnotation(selectedMarker)
        zoomToCoordinate(location.coordinate)
    }
    
    private func zoomToCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapsView.setRegion(region, animated: true)
    }
    
    private func convertAddressToCoordinate(with address: String) {
        HUDManager.shared.show()
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            HUDManager.shared.dismiss()
            guard let placemarks = placemarks, let location = placemarks.first?.location else { return }
            self.updateSelectedMarker(location: location)
        }
    }
    
    private func convertLocationToAddress(with coordinate: CLLocationCoordinate2D) {
        HUDManager.shared.show()
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { [weak self] (placemarks, error) in
            HUDManager.shared.dismiss()
            guard let self = self else { return }
            guard let placemarks = placemarks, let address = placemarks.first, let location = address.location else { return }
            self.address = address.postalAddress?.getFullAddress()
            self.updateSelectedMarker(location: location)
        }
    }
    
    func makeRoute(from coordinateStart: CLLocationCoordinate2D, to coordinateEnd: CLLocationCoordinate2D)  {
        
        HUDManager.shared.show()
        let startPlaceMark = MKPlacemark(coordinate: coordinateStart)
        let endPlaceMark = MKPlacemark(coordinate: coordinateEnd)

        let startMapItem = MKMapItem(placemark: startPlaceMark)
        let endMapItem = MKMapItem(placemark: endPlaceMark)

        let directionRequest = MKDirections.Request()
        directionRequest.source = startMapItem
        directionRequest.destination = endMapItem
        directionRequest.transportType = .automobile

        let directions = MKDirections(request: directionRequest)
        directions.calculate { (routeResponse, routeError) in
            HUDManager.shared.dismiss()
            guard let routeResponse = routeResponse else {
                if let routeError = routeError {
                    print(routeError)
                }
                return
            }
            self.mapsView.removeOverlays(self.mapsView.overlays)
            let route = routeResponse.routes[0]
            self.mapsView.addOverlay(route.polyline, level: .aboveRoads)
            
            //zoom camera map to route
            self.mapsView.visibleMapRect = self.mapsView.mapRectThatFits(route.polyline.boundingMapRect)
        }
    }
    
    private func updateCurrentMarker(cooradinate: CLLocationCoordinate2D) {
        self.currentMarker = MKPointAnnotation()
        self.currentMarker?.coordinate = cooradinate
        self.currentMarker?.title = "My Location"
        self.mapsView.addAnnotation(self.currentMarker!)
    }
    
    //MAKR: - Actions
    @IBAction func didClickMyLocation(_ sender: Any) {
        locationManager.startUpdatingLocation()
    }
}

//MAKR: - MKMapViewDelegate
extension MapsVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation?.title != selectedMarker.title {
            return
        }
        
        let vc = PopupController.createConfirmPopup(with: "Bạn có muốn dẫn đường đến địa điểm này?", okHandler: { [weak self] (alertAction) in
            guard let self = self else { return }
            guard let cooradinateStart = self.locationManager.location?.coordinate else {
                return
            }
            
            //remove all overlays
            self.mapsView.overlays.forEach {
                self.mapsView.removeOverlay($0)
            }
            
            //remove current marker
            if let currentMarker = self.currentMarker {
                self.mapsView.removeAnnotation(currentMarker)
                self.currentMarker = nil
            }
            
            self.updateCurrentMarker(cooradinate: cooradinateStart)
            
            let cooradinateEnd = self.selectedMarker.coordinate
            
            self.makeRoute(from: cooradinateStart, to: cooradinateEnd)
        }, cancelHandler: nil)
        self.present(vc, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let pr = MKPolylineRenderer(overlay: overlay);
        pr.strokeColor = .defaultColor;
        pr.lineWidth = 5;
        return pr;
    }
}

//MAKR: - CLLocationManagerDelegate
extension MapsVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            if isSelectedAddress {
                convertLocationToAddress(with: location.coordinate)
                zoomToCoordinate(location.coordinate)
            }
        }
    }
}
