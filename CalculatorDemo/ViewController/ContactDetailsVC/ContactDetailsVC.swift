//
//  ViewController.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/15/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MessageUI

class ContactDetailsVC: UIViewController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var phoneWorkView: UIView!
    @IBOutlet weak var avatarView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneWorkLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var contact: ContactModel?

    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Helper
    private func setupUI() {
        self.navigationItem.title = "Details"
        
        if let contact = contact {
            nameLabel.text = contact.name
            phoneLabel.text = contact.phone
            phoneWorkLabel.text = contact.phoneWork
            addressLabel.text = contact.address
            emailLabel.text = contact.mail
            addressView.isHidden = contact.address == ""
            phoneWorkView.isHidden = contact.phoneWork == ""
            
            getAvatar()
        }
        
        //Add shadow
        infoView.layer.shadowColor = UIColor.lightGray.cgColor
        infoView.layer.shadowRadius = 5
        infoView.layer.shadowOffset = .zero
        infoView.layer.shadowOpacity = 1
        
        avatarView.addShadow(color: .lightGray, radius: 8, opacity: 1, offset: .zero)
        
        //Add action for address view
        let tapAddress = UITapGestureRecognizer(target: self, action: #selector(didClickAddress))
        addressView.addGestureRecognizer(tapAddress)
    }
    
    @objc func didClickAddress() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapsVC") as? MapsVC {
            vc.address = contact?.address
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: - Actions
    @IBAction func didClickMessageButton(_ sender: Any) {
        guard let phone = contact?.phone else { return }
        let sms: String = "sms:\(phone)&body="
        let strURL: String = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        UIApplication.shared.open(URL.init(string: strURL)!, options: [:], completionHandler: nil)    }
    
    @IBAction func didClickCallButton(_ sender: Any) {
        if let phone = contact?.phone, let url = URL(string: "tel://\(phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    //MARK: - APIs
    private func getAvatar() {
        guard let avatar = contact?.avatar, avatar != "" else {
            return
        }
        APIController.shared.getImage(urlString: avatar) { [weak self] (avatar) in
            guard let self = self else { return }
            if let avatar = avatar {
                DispatchQueue.main.async {
                    self.avatarImageView.image = avatar
                }
            }
        }
    }

}

