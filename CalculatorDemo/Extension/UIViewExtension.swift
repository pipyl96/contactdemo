//
//  UIViewExtension.swift
//  CalculatorDemo
//
//  Created by unitech on 10/5/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(color: UIColor, radius: CGFloat, opacity: Float, offset: CGSize) {
        self.clipsToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
    }
}
