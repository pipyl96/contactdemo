//
//  Extension.swift
//  CalculatorDemo
//
//  Created by unitech on 10/5/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import Contacts

extension CNPostalAddress {
    func getFullAddress() -> String {
        var address = ""
        if self.street != "" {
            address += self.street + ", "
        }
        if self.subLocality != "" {
            address += self.subLocality + ", "
        }
        if self.subAdministrativeArea != "" {
            address += self.subAdministrativeArea + ", "
        }
        if self.state != "" {
            address += self.state
        }
        return address
    }
}
